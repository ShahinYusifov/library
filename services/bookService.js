const Book = require("../models/bookModel");
const Publisher = require("../models/publisherModel");
const Author = require("../models/authorModel");


const deleteBook = async (req, res) => {
  try {
    const id = req.params.id;
    await Book.update({is_deleted: true},{ where: { id } });
    res.status(200).json({
      succeeded: true,
      message: "Library deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error,
    });
  }
};

const updateBook = async (req, res) => {
  const data = req.body;
  try {
    await Book.update(
      data,
      { where: { id: req.params.id } }
    );

    res.status(200).json({
      succeeded: true,
      message: "Updated successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const createBook = async (req, res) => {
  const data = req.body;

  try {
    await Book.create(data);
    res.status(200).json({
      succeeded: true,
      message: "Book is created successfully",
    });
  } catch (error) {
    let result = {};

    if (error.name === "SequelizeValidationError") {
      Object.keys(error.errors).forEach((key) => {
        result[key] = error.errors[key].message;
      });
    }

    res.status(400).json(result);
  }
};

const getABook = async (req, res) => {
  try {
    const book = await Book.findOne({ where: { id: req.params.id } });

    if (!book) {
      return res.status(404).json({
        succeeded: false,
        message: "Library not found",
      });
    }

    res.status(200).json({ book });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAllBooks = async (req, res) => {
  try {
    const book = await Book.findAll({include: Publisher, include: Author});

    res.status(200).json({ book });
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error,
    });
  }
};

module.exports = {
  getABook,
  getAllBooks,
  updateBook,
  createBook,
  deleteBook
};
