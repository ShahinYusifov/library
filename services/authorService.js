const Author = require('../models/authorModel');

const deleteAuthor = async (req, res) => {
  try {
    const id = req.params.id;
    await Author.update({is_deleted: true},{where: {id}})
    res.status(200).json({
      succeeded: true,
      message: "Author deleted successfully"
    })
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error
    })
  }
}

const updateAuthor = async (req,res)=>{
  const { fullname,photo,birthDate,link,info } = req.body;
  try {
    await Author.update({fullname,photo,birthDate,link,info},{where:{id:req.params.id}})
  
    res.status(200).json({
      succeeded: true,
      message: "Updated successfully"
    }) 
    
  } catch (error) {
    res.status(400).json({
      succeeded:false,
      error
    })
  }
}


const createAuthor = async (req,res)=>{
  const {fullname,photo,birthDate,link,info} = req.body
  try {
    await Author.create({fullname,photo,birthDate,link,info})
    res.status(200).json({
      succeeded: true,
      message: "Author is created successfully"
    })
  } catch (error) {
    let result = {};

    if (error.name === 'SequelizeValidationError') {
      Object.keys(error.errors).forEach((key) => {
        result[key] = error.errors[key].message;
      });
    }

    res.status(400).json(result);
  }
}

const getAAuthor = async (req,res)=>{
  try {
    const author = await Author.findOne({where: {id: req.params.id}})

    if(!author) {
      return res.status(404).json({
        succeeded: false,
        message: 'Author not found'
      })
    }

    res.status(200).json({author})
  } catch (error) {
    res.status(400).json({
      succeeded:false,
      error
    })
  }
}


const getAllAuthors = async (req,res)=>{
  try {
    const author = await Author.findAll()

    res.status(200).json({author})
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error
    })
  }
}



module.exports = {
  getAAuthor,
  getAllAuthors,
  createAuthor,
  updateAuthor,
  deleteAuthor
}