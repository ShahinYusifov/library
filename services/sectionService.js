const Library = require("../models/libraryModel");
const Section = require("../models/sectionModel");
const Shelf = require("../models/shelfModel");

const deleteSection = async (req, res) => {
  try {
    const id = req.params.id;
    await Section.update({is_deleted: true},{ where: { id } });
    res.status(200).json({
      succeeded: true,
      message: "User deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error,
    });
  }
};

const updateSection = async (req, res) => {
  const { name, surname, birthDate, password, email, phone } = req.body;
  try {
    await Section.update(
      { name, surname, birthDate, password, email, phone },
      { where: { id: req.params.id } }
    );

    res.status(200).json({
      succeeded: true,
      message: "Updated successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const createSection = async (req, res) => {
  const { name, address } = req.body;

  const userId = res.locals.user.dataValues.id;

  const library = await Library.findOne({where: {userId}})
  
  try {
 
    await Section.create({ name, address, libraryId: library.dataValues.id });
    res.status(200).json({
      succeeded: true,
      message: "Section is created successfully",
    });
  } catch (error) {
    let result = {};

    if (error.name === "SequelizeValidationError") {
      Object.keys(error.errors).forEach((key) => {
        result[key] = error.errors[key].message;
      });
    }

    res.status(400).json(result);
  }
};

const getASection = async (req, res) => {
  try {
    const user = await Section.findOne({ where: { id: req.params.id } });

    if (!user) {
      return res.status(404).json({
        succeeded: false,
        message: "Section not found",
      });
    }

    res.status(200).json({ user });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAllSections = async (req, res) => {
  const userId = res.locals.user.dataValues.id;

  const library = await Library.findOne({where: {userId}})
  try {
    const section = await Section.findAll({where: {libraryId: library.dataValues.id}},{include: Shelf});

    res.status(200).json({ section });
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error,
    });
  }
};

module.exports = {
  getASection,
  getAllSections,
  createSection,
  updateSection,
  deleteSection,
};
