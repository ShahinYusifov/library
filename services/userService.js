const User = require("../models/userModel");
const Library = require("../models/libraryModel");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')

const registerUser = async (req, res) => {
  const { name, surname, birthDate, password, email, phone } = req.body;

  const user = await User.findOne({where: {email}})
try {
  if(user) {
    return res.status(401).json({
      success: false,
      message: "This email is already registered",
  });
}

  await User.create({ name, surname, birthDate, password, email, phone })  
  res.status(201).json({
    succeded: true,
    message: "Registration successfully"
  })
  } catch (error) {

    let result = {};

    if (error.name === 'SequelizeUniqueConstraintError') {
      Object.keys(error.errors).forEach((key) => {
        result[error.errors[key].path] = error.errors[key].message;
      });
    }

    if (error.name === 'SequelizeValidationError') {
      Object.keys(error.errors).forEach((key) => {
        result[error.errors[key].path] = error.errors[key].message;
      });
    }
    console.log(result);

    res.status(400).json(result);
  }
}

const loginUser = async (req, res)=>{
  try {
    const {email, password} = req.body;

    const user = await User.findOne({where:{email}})

    let same = false;

    if(user) {
      same = await bcrypt.compare(password,user.password)
    } else {
      return res.status(401).send({
        succeded: false,
        error: 'There is no such user',
      })
    }

    if(same){

      const token = createToken(user.id)
      res.cookie("jwt",token, {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24,
      })
      res.status(200).json({
        user,
      })
    } else {
      return res.status(400).send({
        succeded: false,
        message: "Invalid password",
      })
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      succeded: false,      
    });
  }
};

const createToken = (userId) => {
  return jwt.sign({ userId }, process.env.JWT_SECRET_KEY, {
    expiresIn: '1d',
  });
};

const deleteUser = async (req, res) => {
  try {
    const id = req.params.id;
    await User.update({is_deleted: true,},{ where: { id } });
    res.status(200).json({
      succeeded: true,
      message: "User deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error,
    });
  }
};

const updateUser = async (req, res) => {
  const { name, surname, birthDate, password, email, phone } = req.body;
  try {
    await User.update(
      { name, surname, birthDate, password, email, phone },
      { where: { id: req.params.id } }
    );

    res.status(200).json();
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAUser = async (req, res) => {
  try {
    const user = await User.findOne({ where: { id: req.params.id } });

    if (!user) {
      return res.status(404).json({
        succeeded: false,
        message: "User not found",
      });
    }

    res.status(200).json({ user });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAllUsers = async (req, res) => {
  
  try {
    const user = await User.findAll({include: Library},);

    res.status(200).json({ user });
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error,
    });
  }
};

module.exports = {
  registerUser,
  loginUser,
  getAllUsers,
  getAUser,
  updateUser,
  deleteUser,
};
