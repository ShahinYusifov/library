const Library = require("../models/libraryModel");
const Section = require("../models/sectionModel");


const deleteLibrary = async (req, res) => {
  try {
    const id = req.params.id;
    await Library.update({is_deleted: true},{ where: { id } });
    res.status(200).json({
      succeeded: true,
      message: "Library deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error,
    });
  }
};

const updateLibrary = async (req, res) => {
  const { name, address, photo } = req.body;
  try {
    await Library.update(
      { name, address, photo },
      { where: { id: req.params.id } }
    );

    res.status(200).json({
      succeeded: true,
      message: "Updated successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const createLibrary = async (req, res) => {
  const { name, address, photo } = req.body;
  const userId = res.locals.user.dataValues.id;
  
  try {
    await Library.create({ name, address, photo,userId });
    res.status(200).json({
      succeeded: true,
      message: "Library is created successfully",
    });
  } catch (error) {
    let result = {};

    if (error.name === "SequelizeValidationError") {
      Object.keys(error.errors).forEach((key) => {
        result[key] = error.errors[key].message;
      });
    }

    res.status(400).json(result);
  }
};

const getALibrary = async (req, res) => {
  try {
    const library = await Library.findOne({ where: { id: req.params.id } });

    if (!library) {
      return res.status(404).json({
        succeeded: false,
        message: "Library not found",
      });
    }

    res.status(200).json({ library });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAllLibraries = async (req, res) => {
  const userId = res.locals.user.dataValues.id;
  try { 

    const library = await Library.findAll({where: {userId}},{include: Section});  

    res.status(200).json({ library });
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error,
    });
  }
};

module.exports = {
  getALibrary,
  getAllLibraries,
  createLibrary,
  updateLibrary,
  deleteLibrary,
};
