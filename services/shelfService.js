const Book = require("../models/bookModel");
const Shelf = require("../models/shelfModel");

const deleteShelf = async (req, res) => {
  try {
    const id = req.params.id;
    await Shelf.update({is_deleted: true},{ where: { id } });
    res.status(200).json({
      succeeded: true,
      message: "Shelf deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error,
    });
  }
};

const updateShelf = async (req, res) => {
  const { name } = req.body;
  try {
    await Shelf.update({ name }, { where: { id: req.params.id } });

    res.status(200).json({
      succeeded: true,
      message: "Updated successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const createShelf = async (req, res) => {
  const { name } = req.body;
  try {
    await Shelf.create({ name });
    res.status(200).json({
      succeeded: true,
      message: "Shelf is created successfully",
    });
  } catch (error) {
    let result = {};

    if (error.name === "SequelizeValidationError") {
      Object.keys(error.errors).forEach((key) => {
        result[key] = error.errors[key].message;
      });
    }

    res.status(400).json(result);
  }
};

const getAShelf = async (req, res) => {
  try {
    const shelf = await Shelf.findOne({ where: { id: req.params.id } });

    if (!shelf) {
      return res.status(404).json({
        succeeded: false,
        message: "Shelf not found",
      });
    }

    res.status(200).json({ shelf });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAllshelfs = async (req, res) => {
  try {
    const shelf = await Shelf.findAll({include: Book});

    res.status(200).json({ shelf });
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error,
    });
  }
};

module.exports = {
  getAShelf,
  getAllshelfs,
  createShelf,
  updateShelf,
  deleteShelf,
};
