const getIndexPage = (req, res) => {
  res.status(200).send("Home Page");
};

const getRegisterPage = (req, res) => {
  res.status(200).send("Register Page");
};

const getLoginPage = (req, res) => {
  res.status(200).send("Login Page");
};

const getLogout = (req, res) => {
  res.cookie('jwt',"", {
    maxAge:1  
  })
  res.send("logged out");
}

module.exports = {getIndexPage,getRegisterPage,getLoginPage, getLogout}
