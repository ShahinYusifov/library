const Publisher = require("../models/publisherModel");

const deletePublisher = async (req, res) => {
  try {
    const id = req.params.id;
    await Publisher.update({is_deleted: true},{ where: { id } });
    res.status(200).json({
      succeeded: true,
      message: "Publisher deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      message: error,
    });
  }
};

const updatePublisher = async (req, res) => {
  const { name } = req.body;
  try {
    await Publisher.update({ name }, { where: { id: req.params.id } });

    res.status(200).json({
      succeeded: true,
      message: "Updated successfully",
    });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const createPublisher = async (req, res) => {
  const { name } = req.body;
  try {
    await Publisher.create({ name });
    res.status(200).json({
      succeeded: true,
      message: "Publisher is created successfully",
    });
  } catch (error) {
    let result = {};

    if (error.name === "SequelizeValidationError") {
      Object.keys(error.errors).forEach((key) => {
        result[key] = error.errors[key].message;
      });
    }

    res.status(400).json(result);
  }
};

const getAPublihser = async (req, res) => {
  try {
    const publisher = await Publisher.findOne({ where: { id: req.params.id } });

    if (!publisher) {
      return res.status(404).json({
        succeeded: false,
        message: "Publisher not found",
      });
    }

    res.status(200).json({ publisher });
  } catch (error) {
    res.status(400).json({
      succeeded: false,
      error,
    });
  }
};

const getAllPublihsers = async (req, res) => {
  try {
    const publisher = await Publisher.findAll();

    res.status(200).json({ publisher });
  } catch (error) {
    res.status(404).json({
      succeeded: false,
      error,
    });
  }
};

module.exports = {
  getAPublihser,
  getAllPublihsers,
  createPublisher,
  updatePublisher,
  deletePublisher,
};
