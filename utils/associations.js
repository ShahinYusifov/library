const User = require('../models/userModel');
const Library = require('../models/libraryModel');
const Section = require('../models/sectionModel');
const Shelf = require('../models/shelfModel');
const Book = require('../models/bookModel');
const Author = require('../models/authorModel');
const Publisher = require('../models/publisherModel');


User.hasMany(Library);
Library.belongsTo(User);
Library.hasMany(Section);
Section.belongsTo(Library);
Section.hasMany(Shelf);
Shelf.belongsTo(Section);
Shelf.hasMany(Book);
Book.belongsTo(Shelf);
Author.hasMany(Book);
Publisher.hasMany(Book);