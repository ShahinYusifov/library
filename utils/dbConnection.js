const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("library", "root", "", {
  host: "localhost",
  dialect: "mysql",
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established succesfully");
  })
  .catch((error) => {
    console.log("Unable to connect to database", error);
  });

module.exports = sequelize;
