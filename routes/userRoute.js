const express = require("express");
const router = express.Router();
const userService = require("../services/userService");
const { authenticateToken } = require("../middlewares/authMiddleware");

router.put("/:id", authenticateToken,  userService.deleteUser);

router.put("/:id", authenticateToken, userService.updateUser);

router.get("/:id",authenticateToken,  userService.getAUser);

router.get("/",authenticateToken, userService.getAllUsers);

router.post('/register', userService.registerUser);

router.post("/login", userService.loginUser);



module.exports = router;
