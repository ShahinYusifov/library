const express = require("express");
const router = express.Router();
const authorService = require("../services/authorService");

router.put("/:id", authorService.deleteAuthor);

router.put("/:id", authorService.updateAuthor);

router.post("/", authorService.createAuthor);

router.get("/:id", authorService.getAAuthor);

router.get("/", authorService.getAllAuthors);

module.exports = router;
