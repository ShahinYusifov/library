const express = require("express");
const router = express.Router();
const bookService = require("../services/bookService");

router.put("/:id", bookService.deleteBook);

router.put("/:id", bookService.updateBook);

router.post("/", bookService.createBook);

router.get("/:id", bookService.getABook);

router.get("/", bookService.getAllBooks);

module.exports = router;
