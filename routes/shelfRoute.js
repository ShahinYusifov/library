const express = require("express");
const router = express.Router();
const shelfService = require("../services/shelfService");

router.put("/:id", shelfService.deleteShelf);

router.put("/:id", shelfService.updateShelf);

router.post("/", shelfService.createShelf);

router.get("/:id", shelfService.getAShelf);

router.get("/", shelfService.getAllshelfs);

module.exports = router;
