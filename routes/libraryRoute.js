const express = require("express");
const router = express.Router();
const libraryService = require("../services/libraryService");

router.put("/:id", libraryService.deleteLibrary);

router.put("/:id", libraryService.updateLibrary);

router.post("/", libraryService.createLibrary);

router.get("/:id", libraryService.getALibrary);

router.get("/", libraryService.getAllLibraries);

module.exports = router;
