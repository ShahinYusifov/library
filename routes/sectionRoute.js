const express = require("express");
const router = express.Router();
const sectionService = require("../services/sectionService");

router.put("/:id", sectionService.deleteSection);

router.put("/:id", sectionService.updateSection);

router.post("/", sectionService.createSection);

router.get("/:id", sectionService.getASection);

router.get("/", sectionService.getAllSections);

module.exports = router;
