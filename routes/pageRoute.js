const express = require("express");
const router = express.Router();
const pageService = require("../services/pageService");


router.get("/", pageService.getIndexPage);

router.get("/register", pageService.getRegisterPage);

router.get("/login", pageService.getLoginPage);

router.get("/logout", pageService.getLogout);

module.exports = router;