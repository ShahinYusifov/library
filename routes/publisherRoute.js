const express = require("express");
const router = express.Router();
const publisherService = require("../services/publisherService");

router.put("/:id", publisherService.deletePublisher);

router.put("/:id", publisherService.updatePublisher);

router.post("/", publisherService.createPublisher);

router.get("/:id", publisherService.getAPublihser);

router.get("/", publisherService.getAllPublihsers);

module.exports = router;
