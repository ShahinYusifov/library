const express = require('express');
const app = express();
const dotenv = require("dotenv").config();
const sequelize = require("./utils/dbConnection");
const cookieParser = require('cookie-parser');
const pageRoute = require('./routes/pageRoute');
const userRoute = require('./routes/userRoute')
const libraryRoute = require('./routes/libraryRoute');
const bookRoute = require('./routes/bookRoute');
const sectionRoute = require('./routes/sectionRoute');
const authorRoute = require('./routes/authorRoute');
const publisherRoute = require('./routes/publisherRoute');
const shelfRoute = require('./routes/shelfRoute');
const {checkUser} = require('./middlewares/authMiddleware');
const associate = require('./utils/associations');  




app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());


app.use("*", checkUser);
app.use( pageRoute );
app.use('/users', userRoute);
app.use('/library', libraryRoute);
app.use('/books', bookRoute);
app.use('/section', sectionRoute);
app.use('/author', authorRoute);
app.use('/publisher',publisherRoute);
app.use('/shelf',shelfRoute);


const port = process.env.PORT;

sequelize.sync()
  .then(() => {
    app.listen(port, () => {
      console.log(`Server is listening on ${port}`);
    });
  })
  .catch((error) => {
    console.log(error);
  });
