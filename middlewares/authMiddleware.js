const jwt = require('jsonwebtoken')
const User = require('../models/userModel');

const checkUser = async (req, res, next) => {
    
  const token = req.cookies.jwt;

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET_KEY, async (err, decodedToken) => {
      if (err) {
        console.log(err.message);
        res.locals.user = null;
        next();
      } else {
        const user = await User.findByPk(decodedToken.userId);
        res.locals.user = user;
        next();
      }
    });
  } else {
    res.locals.user = null;
    next();
  }
};

const authenticateToken = async (req, res, next) => {

  try {
    const token = req.cookies.jwt;

    if (token) {
      jwt.verify(token, process.env.JWT_SECRET_KEY, (err) => {
        if (err) {
          console.log(err.message);
        } else {
          next();
        }
      });
    } else {
      res.send('Not authorized');
    }
  } catch (error) {
    res.status(401).json({
      succeeded: false,
      error: 'Not authorized',
    });
  }
};

module.exports = { authenticateToken, checkUser }