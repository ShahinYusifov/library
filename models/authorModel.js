const { DataTypes } = require("sequelize");
const sequelize = require("../utils/dbConnection");

const Author = sequelize.define("author", {
  fullname: {
    type:DataTypes.STRING,
    required: true,
    allowNull: false,
  },
  photo: {
    type: DataTypes.STRING,
  },
  birthDate: {
    type: DataTypes.DATE,
  },
  link: {
    type: DataTypes.STRING,
  },
  info: {
    type: DataTypes.TEXT,
  },
  is_deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  }
},{
  timestamps: false
});

sequelize.sync().then(()=>{
  console.log("Users table created successfully");
}).catch((error)=>{
  console.log("Unable to create users table:" , error);
});

module.exports = Author;