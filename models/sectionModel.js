const { DataTypes } = require("sequelize");
const sequelize = require("../utils/dbConnection");

const Section = sequelize.define("section", {
  name: {
    type:DataTypes.STRING,
    required: true,
    allowNull: false,
    validate: {
      notNull: {
        msg: "Name must be required"
      }
    }
  },
  address: {
    type: DataTypes.STRING,
    required: true,
    allowNull: false,
    validate: {
      notNull: {
        msg: "Address must be required"
      }
    }
  },
  is_deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  }
},{
  timestamps: false,
  validate: true,
});

sequelize.sync( ).then(()=>{
  console.log("Users table created successfully");
}).catch((error)=>{
  console.log("Unable to create users table:" , error);
});

module.exports = Section;