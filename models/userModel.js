const { DataTypes } = require("sequelize");
const sequelize = require("../utils/dbConnection");
const bcrypt = require('bcrypt');

const User = sequelize.define(
  "user",
  {
    name: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Name must not be empty"
        }
      }
    },
    surname: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false, 
      validate: {
        notNull: {
          msg: "Surname must not be empty"
        }
      }  
    },
    birthDate: {
      type: DataTypes.DATEONLY,
      required: true,
      allowNull: false,
      validate: {
        notNull: {
          msg: "Birth date must not be empty"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
      validate: {
        len: { 
           args: [4, 20],
           msg: "The password length should be between 4 and 20 characters."
        }
     }
    },
    email: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
      unique: true,
      validate:{
        isEmail: {
          args: true,
          msg: "Email address must be a valid"
        },
      }
    },
    phone: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false, 
      unique: {msg:"This phone number is already in use"},
      validate: {
        len: { 
           args: [10, 10],
           msg: "Enter valid a phone number"
        },
        isInt: true,
     }
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    }
  },
  {
    timestamps: false,
    validate:true,
  },
);

User.addHook(
  "beforeCreate",
  user => (user.password = bcrypt.hashSync(user.password, 10))
);

sequelize
  .sync()
  .then(() => {
    console.log("Users table created successfully");
  })
  .catch((error) => {
    console.log("Unable to create users table:", error);
  });

module.exports = User;
