const { DataTypes } = require("sequelize");
const sequelize = require("../utils/dbConnection");
const User = require('./userModel');

const Library = sequelize.define("library", {
  name: {
    type:DataTypes.STRING,
    required: true,
    allowNull: false,
  },
  address: {
    type: DataTypes.STRING,
  },
  photo: {
    type: DataTypes.STRING,
  },
  is_deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  }
},{
  timestamps: false,
});

Library.associate = models => {
  Library.hasMany(models.User);
}

sequelize.sync({alter: true}).then(()=>{
  console.log("Users table created successfully");
}).catch((error)=>{
  console.log("Unable to create users table:" , error);
});

module.exports = Library;