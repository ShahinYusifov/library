const { DataTypes } = require("sequelize");
const sequelize = require("../utils/dbConnection");

const Book = sequelize.define("book", {
  name: {
    type:DataTypes.STRING,
    required: true,
    allowNull: false,
  },
  photo: {
    type: DataTypes.STRING,
  },
  date: {
    type: DataTypes.DATE,
  },
  note: {
    type: DataTypes.TEXT,
  },
  info: {
    type: DataTypes.TEXT,
  },
  is_deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  }
},{
  timestamps: false
});

sequelize.sync().then(()=>{
  console.log("Users table created successfully");
}).catch((error)=>{
  console.log("Unable to create users table:" , error);
});

module.exports = Book;