const { DataTypes } = require("sequelize");
const sequelize = require("../utils/dbConnection");

const Publisher = sequelize.define("publisher", {
  name: {
    type:DataTypes.STRING,
    required: true,
    allowNull: false,
  },
  is_deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  }
},{
  timestamps: false
});

sequelize.sync().then(()=>{
  console.log("Users table created successfully");
}).catch((error)=>{
  console.log("Unable to create users table:" , error);
});

module.exports = Publisher;